package xyz.nergal.proca.common.data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class PageableQueryParameterBuilderTest {

    @Test
    void build() {
        PageableQueryParameterBuilder builder = new PageableQueryParameterBuilder();

        Pageable pageable = PageRequest.of(1, 20);
        Assertions.assertEquals("page=1&size=20", builder.build(pageable));
    }

    @Test
    void buildHandlesNull() {
        PageableQueryParameterBuilder builder = new PageableQueryParameterBuilder();

        Assertions.assertNull(builder.build(null));
    }

    @Test
    void buildHandlesUnpaged() {
        PageableQueryParameterBuilder builder = new PageableQueryParameterBuilder();

        Assertions.assertNull(builder.build(Pageable.unpaged()));
    }

    @Test
    void buildWithSort() {
        PageableQueryParameterBuilder builder = new PageableQueryParameterBuilder();

        Sort sort = Sort.by(Sort.Order.desc("p1"), Sort.Order.asc("p2"));
        Pageable pageable = PageRequest.of(1, 20, sort);
        Assertions.assertEquals("page=1&size=20&sort=p1,DESC&sort=p2,ASC", builder.build(pageable));
    }

    @Test
    void modifiableParameterNames() {
        PageableQueryParameterBuilder builder = new PageableQueryParameterBuilder("p", "s", "q");

        Sort sort = Sort.by(Sort.Order.desc("p1"), Sort.Order.asc("p2"));
        Pageable pageable = PageRequest.of(1, 20, sort);
        Assertions.assertEquals("p=1&s=20&q=p1,DESC&q=p2,ASC", builder.build(pageable));
    }
}
