package xyz.nergal.proca.common.databind;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PageJsonComponentTest {

    @Test
    void deserializerHasCorrectType() {
        Assertions.assertTrue(new PageJsonComponent.Deserializer() instanceof PageJsonDeserializer);
    }

    @Test
    void serializerHasCorrectType() {
        Assertions.assertTrue(new PageJsonComponent.Serializer() instanceof PageJsonSerializer);
    }
}
