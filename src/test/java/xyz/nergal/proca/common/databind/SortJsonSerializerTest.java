package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class SortJsonSerializerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Sort.class, new SortJsonSerializer());
        objectMapper.registerModule(simpleModule);
    }

    @Test
    void handledType() {
        SortJsonSerializer serializer = new SortJsonSerializer();

        Assertions.assertEquals(Sort.class, serializer.handledType());
    }

    @Test
    void isEmpty() {
        SortJsonSerializer serializer = new SortJsonSerializer();

        Assertions.assertTrue(serializer.isEmpty(null, Sort.unsorted()));
        Assertions.assertFalse(serializer.isEmpty(null, Sort.by("p")));
    }

    @Test
    void serialize() throws IOException, JSONException {
        Sort sort = Sort.by(
                Sort.Order.asc("p1"),
                Sort.Order.desc("p2")
        );

        String actual = objectMapper.writeValueAsString(sort);

        Assertions.assertNotNull(actual);

        Resource resource = new ClassPathResource("static/sort-payload.json");
        String expected = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);

        JSONAssert.assertEquals(expected, actual, true);
    }
}
