package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class PageJsonDeserializerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(Page.class, new PageJsonDeserializer());
        simpleModule.addDeserializer(Sort.class, new SortJsonDeserializer());
        objectMapper.registerModule(simpleModule);
    }

    @ParameterizedTest
    @ValueSource(strings = {"[]", "\"\"", "1"})
    void deserializeInvalidValues(String content) {
        Assertions.assertThrows(MismatchedInputException.class, () -> {
            objectMapper.readValue(content, Page.class);
        });
    }

    @Test
    void deserialize() throws IOException {
        Resource resource = new ClassPathResource("static/page-payload.json");
        String payload = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);

        Page actual = objectMapper.readValue(payload, new TypeReference<Page<Object>>() {
        });

        Assertions.assertNotNull(actual);

        Map<Object, Object> map = new HashMap<>();
        map.put("p1", 1);
        map.put("p2", "string");

        Page expected = new PageImpl<>(
                Collections.singletonList(map),
                PageRequest.of(1, 20),
                21
        );

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void deserializeEmpty() throws IOException {
        Page actual = objectMapper.readValue("{}", new TypeReference<Page<Object>>() {
        });

        Assertions.assertNotNull(actual);

        Page expected = new PageImpl<>(
                Collections.emptyList(),
                PageRequest.of(0, 1),
                0
        );

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void deserializeWithSort() throws IOException {
        Resource resource = new ClassPathResource("static/page-with-sort-payload.json");
        String payload = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);

        Page actual = objectMapper.readValue(payload, new TypeReference<Page<Object>>() {
        });

        Assertions.assertNotNull(actual);

        Map<Object, Object> map = new HashMap<>();
        map.put("p1", 1);
        map.put("p2", "string");

        Page expected = new PageImpl<>(
                Collections.singletonList(map),
                PageRequest.of(1, 20, Sort.by(Sort.Order.asc("p1"))),
                21
        );

        Assertions.assertEquals(expected, actual);
    }
}
