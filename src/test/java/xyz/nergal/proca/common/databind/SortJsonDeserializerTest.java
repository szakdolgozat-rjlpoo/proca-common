package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

class SortJsonDeserializerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(Sort.class, new SortJsonDeserializer());
        objectMapper.registerModule(simpleModule);
    }

    @ParameterizedTest
    @ValueSource(strings = {"{}", "\"\"", "1"})
    void deserializeInvalidValues(String content) {
        Assertions.assertThrows(MismatchedInputException.class, () -> {
            objectMapper.readValue(content, Sort.class);
        });
    }

    @Test
    void deserializeEmpty() throws IOException {
        Sort actual = objectMapper.readValue("[]", Sort.class);

        Assertions.assertNotNull(actual);

        Sort expected = Sort.unsorted();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void deserializeSkipsNullValues() throws IOException {
        Sort actual = objectMapper.readValue("[null, null]", Sort.class);

        Assertions.assertNotNull(actual);

        Sort expected = Sort.unsorted();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void deserialize() throws IOException {
        Resource resource = new ClassPathResource("static/sort-payload.json");
        String payload = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);

        Sort actual = objectMapper.readValue(payload, Sort.class);

        Assertions.assertNotNull(actual);

        Sort expected = Sort.by(
                Sort.Order.asc("p1"),
                Sort.Order.desc("p2")
        );

        Assertions.assertEquals(expected, actual);
    }
}
