package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

class PageableJsonDeserializerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(Pageable.class, new PageableJsonDeserializer());
        simpleModule.addDeserializer(Sort.class, new SortJsonDeserializer());
        objectMapper.registerModule(simpleModule);
    }

    @ParameterizedTest
    @ValueSource(strings = {"[]", "\"\"", "1"})
    void deserializeInvalidValues(String content) {
        Assertions.assertThrows(MismatchedInputException.class, () -> {
            objectMapper.readValue(content, Pageable.class);
        });
    }

    @Test
    void deserializeEmpty() throws IOException {
        Pageable actual = objectMapper.readValue("{}", Pageable.class);

        Assertions.assertNotNull(actual);

        Pageable expected = PageRequest.of(0, 20);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void deserialize() throws IOException {
        Resource resource = new ClassPathResource("static/pageable-payload.json");
        String payload = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);

        Pageable actual = objectMapper.readValue(payload, Pageable.class);

        Assertions.assertNotNull(actual);

        Pageable expected = PageRequest.of(0, 20);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void deserializeWithSort() throws IOException {
        Resource resource = new ClassPathResource("static/pageable-with-sort-payload.json");
        String payload = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);

        Pageable actual = objectMapper.readValue(payload, Pageable.class);

        Assertions.assertNotNull(actual);

        Pageable expected = PageRequest.of(0, 20, Sort.by(Sort.Order.asc("p1")));

        Assertions.assertEquals(expected, actual);
    }
}
