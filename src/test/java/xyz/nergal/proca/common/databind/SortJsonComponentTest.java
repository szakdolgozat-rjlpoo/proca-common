package xyz.nergal.proca.common.databind;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SortJsonComponentTest {

    @Test
    void deserializerHasCorrectType() {
        Assertions.assertTrue(new SortJsonComponent.Deserializer() instanceof SortJsonDeserializer);
    }

    @Test
    void serializerHasCorrectType() {
        Assertions.assertTrue(new SortJsonComponent.Serializer() instanceof SortJsonSerializer);
    }
}
