package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class PageJsonSerializerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(Page.class, new PageJsonSerializer());
        simpleModule.addSerializer(Sort.class, new SortJsonSerializer());
        objectMapper.registerModule(simpleModule);
    }

    @Test
    void handledType() {
        PageJsonSerializer serializer = new PageJsonSerializer();

        Assertions.assertEquals(Page.class, serializer.handledType());
    }

    @Test
    void isEmpty() {
        PageJsonSerializer serializer = new PageJsonSerializer();

        Assertions.assertTrue(serializer.isEmpty(null, Page.empty()));
        Assertions.assertFalse(serializer.isEmpty(null, new PageImpl<>(
                Collections.singletonList(new Object()),
                PageRequest.of(1, 20),
                21
        )));
    }

    @Test
    void serialize() throws IOException, JSONException {
        Map<Object, Object> map = new HashMap<>();
        map.put("p1", 1);
        map.put("p2", "string");

        Page page = new PageImpl<>(
                Collections.singletonList(map),
                PageRequest.of(1, 20),
                21
        );

        String actual = objectMapper.writeValueAsString(page);

        Assertions.assertNotNull(actual);

        Resource resource = new ClassPathResource("static/page-with-empty-sort-payload.json");
        String expected = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);

        JSONAssert.assertEquals(expected, actual, true);
    }

    @Test
    void serializeWithSort() throws IOException, JSONException {
        Map<Object, Object> map = new HashMap<>();
        map.put("p1", 1);
        map.put("p2", "string");

        Page page = new PageImpl<>(
                Collections.singletonList(map),
                PageRequest.of(1, 20, Sort.by(Sort.Order.asc("p1"))),
                21
        );

        String actual = objectMapper.writeValueAsString(page);

        Assertions.assertNotNull(actual);

        Resource resource = new ClassPathResource("static/page-with-sort-payload.json");
        String expected = IOUtils.toString(resource.getInputStream(), StandardCharsets.UTF_8);

        JSONAssert.assertEquals(expected, actual, true);
    }
}
