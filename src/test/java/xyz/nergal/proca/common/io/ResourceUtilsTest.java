package xyz.nergal.proca.common.io;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

class ResourceUtilsTest {

    @Test
    void asStringShouldReturnResourceAsString() throws IOException {
        String expected = "string";
        Resource resource = new ByteArrayResource(expected.getBytes());

        String actual = ResourceUtils.asString(resource);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void asStringShouldAcceptCharset() throws IOException {
        String expected = "string";
        Resource resource = new ByteArrayResource(expected.getBytes());

        String actual = ResourceUtils.asString(resource, StandardCharsets.UTF_8);

        Assertions.assertEquals(expected, actual);
    }
}
