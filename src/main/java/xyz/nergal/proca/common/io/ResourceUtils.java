package xyz.nergal.proca.common.io;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class ResourceUtils {

    private ResourceUtils() {
    }

    public static String asString(Resource resource) throws IOException {
        return asString(resource, StandardCharsets.UTF_8);
    }

    public static String asString(Resource resource, Charset charset) throws IOException {
        return IOUtils.toString(resource.getInputStream(), charset);
    }
}
