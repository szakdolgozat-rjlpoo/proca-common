package xyz.nergal.proca.common.data;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class PageableQueryParameterBuilder {

    private static final String DEFAULT_PAGE_PARAMETER = "page";
    private static final String DEFAULT_SIZE_PARAMETER = "size";
    private static final String DEFAULT_SORT_PARAMETER = "sort";

    private String pageParameterName = DEFAULT_PAGE_PARAMETER;
    private String sizeParameterName = DEFAULT_SIZE_PARAMETER;
    private String sortParameterName = DEFAULT_SORT_PARAMETER;

    public PageableQueryParameterBuilder() {
    }

    public PageableQueryParameterBuilder(
            String pageParameterName,
            String sizeParameterName,
            String sortParameterName) {
        this.pageParameterName = pageParameterName;
        this.sizeParameterName = sizeParameterName;
        this.sortParameterName = sortParameterName;
    }

    /**
     * Build a query parameter string from the given pageable.
     *
     * @param pageable
     * @return query parameter
     */
    public String build(Pageable pageable) {
        if (pageable == null || pageable == Pageable.unpaged()) {
            return null;
        }

        StringBuilder builder = new StringBuilder();

        builder.append(pageParameterName).append("=").append(pageable.getPageNumber())
                .append('&')
                .append(sizeParameterName).append("=").append(pageable.getPageSize());

        for (Sort.Order order : pageable.getSort()) {
            builder.append('&')
                    .append(sortParameterName)
                    .append("=")
                    .append(order.getProperty())
                    .append(',')
                    .append(order.getDirection());
        }

        return builder.toString();
    }
}
