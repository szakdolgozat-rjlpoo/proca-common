package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PageJsonDeserializer extends JsonDeserializer<Page> implements ContextualDeserializer {

    private static final String CONTENT_FIELD_NAME = "content";
    private static final String NUMBER_FIELD_NAME = "number";
    private static final String SIZE_FIELD_NAME = "size";
    private static final String TOTAL_ELEMENTS_FIELD_NAME = "totalElements";
    private static final String SORT_FIELD_NAME = "sort";

    private JavaType valueType;

    @Override
    public Page<?> deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        if (!jsonParser.isExpectedStartObjectToken()) {
            return (Page<?>) context.handleUnexpectedToken(handledType(), jsonParser);
        }

        return deserializeObject(jsonParser, context);
    }

    private Page deserializeObject(JsonParser jsonParser, DeserializationContext context) throws IOException {
        final CollectionType valuesListType = context.getTypeFactory().constructCollectionType(List.class, valueType);

        List<?> list = new ArrayList<>();
        int pageNumber = 0;
        int pageSize = 1;
        long total = 0;
        Sort sort = Sort.unsorted();

        String fieldName = "";
        JsonToken jsonToken;
        while ((jsonToken = jsonParser.nextToken()) != JsonToken.END_OBJECT) {
            if (JsonToken.FIELD_NAME == jsonToken) {
                fieldName = jsonParser.getCurrentName();
                continue;
            }

            switch (fieldName) {
                case CONTENT_FIELD_NAME:
                    list = context.readValue(jsonParser, valuesListType);
                    break;
                case NUMBER_FIELD_NAME:
                    pageNumber = context.readValue(jsonParser, Integer.class);
                    break;
                case SIZE_FIELD_NAME:
                    pageSize = context.readValue(jsonParser, Integer.class);
                    break;
                case TOTAL_ELEMENTS_FIELD_NAME:
                    total = context.readValue(jsonParser, Long.class);
                    break;
                case SORT_FIELD_NAME:
                    sort = context.readValue(jsonParser, Sort.class);
                    break;
                default:
                    jsonParser.skipChildren();
            }
            fieldName = "";
        }

        return new PageImpl<>(list, PageRequest.of(pageNumber, pageSize, sort), total);
    }

    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext context, BeanProperty property) {
        JavaType wrapperType = context.getContextualType();
        PageJsonDeserializer deserializer = new PageJsonDeserializer();
        deserializer.valueType = wrapperType.containedType(0);
        return deserializer;
    }
}
