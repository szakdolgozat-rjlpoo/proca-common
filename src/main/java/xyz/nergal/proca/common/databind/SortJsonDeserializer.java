package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.data.domain.Sort;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SortJsonDeserializer extends JsonDeserializer<Sort> {

    private static final String PROPERTY_FIELD_NAME = "property";
    private static final String DIRECTION_FIELD_NAME = "direction";

    @Override
    public Sort deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        if (!jsonParser.isExpectedStartArrayToken()) {
            return (Sort) context.handleUnexpectedToken(handledType(), jsonParser);
        }
        List<Sort.Order> orders = new ArrayList<>();

        JsonToken jsonToken;
        while ((jsonToken = jsonParser.nextToken()) != JsonToken.END_ARRAY) {
            if (jsonToken == JsonToken.VALUE_NULL) {
                continue;
            }
            if (!jsonParser.isExpectedStartObjectToken()) {
                return (Sort) context.handleUnexpectedToken(handledType(), jsonParser);
            }
            orders.add(deserializeObject(jsonParser, context));
        }

        return Sort.by(orders);
    }

    private Sort.Order deserializeObject(JsonParser jsonParser, DeserializationContext context) throws IOException {
        Sort.Direction direction = Sort.DEFAULT_DIRECTION;
        String property = "";

        String fieldName = "";
        JsonToken jsonToken;
        while ((jsonToken = jsonParser.nextToken()) != JsonToken.END_OBJECT) {
            if (JsonToken.FIELD_NAME == jsonToken) {
                fieldName = jsonParser.getCurrentName();
                continue;
            }

            switch (fieldName) {
                case PROPERTY_FIELD_NAME:
                    property = context.readValue(jsonParser, String.class);
                    break;
                case DIRECTION_FIELD_NAME:
                    direction = Sort.Direction.fromString(context.readValue(jsonParser, String.class));
                    break;
                default:
                    jsonParser.skipChildren();
            }
            fieldName = "";
        }

        return new Sort.Order(
                direction,
                property
        );
    }
}
