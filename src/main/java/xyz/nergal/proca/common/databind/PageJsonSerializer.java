package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.data.domain.Page;

import java.io.IOException;

public class PageJsonSerializer extends JsonSerializer<Page> {

    @Override
    public void serialize(Page page, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();

        gen.writeObjectField("content", page.getContent());
        gen.writeObjectField("sort", page.getSort());
        gen.writeObjectField("totalElements", page.getTotalElements());
        gen.writeObjectField("size", page.getSize());
        gen.writeObjectField("number", page.getNumber());

        gen.writeEndObject();
    }

    @Override
    public Class<Page> handledType() {
        return Page.class;
    }

    @Override
    public boolean isEmpty(SerializerProvider provider, Page value) {
        return value == null || value.isEmpty();
    }
}
