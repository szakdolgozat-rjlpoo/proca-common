package xyz.nergal.proca.common.databind;

import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
public class PageJsonComponent {

    public static class Deserializer extends PageJsonDeserializer {
    }

    public static class Serializer extends PageJsonSerializer {
    }
}
