package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.IOException;

@JsonComponent
public class PageableJsonDeserializer extends JsonDeserializer<Pageable> {

    private static int defaultPageSize = 20;

    @Override
    public Pageable deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        if (!jsonParser.isExpectedStartObjectToken()) {
            return (Pageable) context.handleUnexpectedToken(this.handledType(), jsonParser);
        }
        return this.deserializeObject(jsonParser, context);
    }

    private Pageable deserializeObject(JsonParser jsonParser, DeserializationContext context) throws IOException {
        int page = 0;
        int size = defaultPageSize;
        Sort sort = Sort.unsorted();
        String fieldName = "";

        JsonToken jsonToken;
        while((jsonToken = jsonParser.nextToken()) != JsonToken.END_OBJECT) {
            if (JsonToken.FIELD_NAME == jsonToken) {
                fieldName = jsonParser.getCurrentName();
                continue;
            }
            switch (fieldName) {
                case "page":
                    page = context.readValue(jsonParser, Integer.class);
                    break;
                case "size":
                    size = context.readValue(jsonParser, Integer.class);
                    break;
                case "sort":
                    sort = context.readValue(jsonParser, Sort.class);
                    break;
                default:
                    jsonParser.skipChildren();
            }
            fieldName = "";
        }
        return PageRequest.of(page, size, sort);
    }
}
