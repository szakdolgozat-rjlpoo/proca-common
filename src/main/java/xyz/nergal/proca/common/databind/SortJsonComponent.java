package xyz.nergal.proca.common.databind;

import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
public class SortJsonComponent {

    public static class Deserializer extends SortJsonDeserializer {
    }

    public static class Serializer extends SortJsonSerializer {
    }
}
