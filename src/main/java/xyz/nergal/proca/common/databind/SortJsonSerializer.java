package xyz.nergal.proca.common.databind;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.data.domain.Sort;

import java.io.IOException;

public class SortJsonSerializer extends JsonSerializer<Sort> {

    @Override
    public void serialize(Sort value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartArray();

        for (Sort.Order order : value) {
            gen.writeStartObject();
            gen.writeObjectField("direction", order.getDirection());
            gen.writeStringField("property", order.getProperty());
            gen.writeEndObject();
        }

        gen.writeEndArray();
    }

    @Override
    public Class<Sort> handledType() {
        return Sort.class;
    }

    @Override
    public boolean isEmpty(SerializerProvider provider, Sort value) {
        return value == null || value.isEmpty();
    }
}
